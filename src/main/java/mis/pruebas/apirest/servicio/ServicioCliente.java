package mis.pruebas.apirest.servicio;
import mis.pruebas.apirest.modelo.Cliente;
import mis.pruebas.apirest.modelo.Cuenta;

import java.util.List;


public interface ServicioCliente {

    //CRUD
    public List<Cliente> obtenerClientes(int pagina, int cantidad);

    //CREATE
    public void insertarClienteNuevo(Cliente cliente);

    //READ
    public Cliente obtenerCliente(String documento);

    //UPDATE
    public void guardarCliente(Cliente cliente);

    //UPDATE
    public void emparcharCliente(Cliente parche);

    //DELETE
    public void borrarCliente(String documento);

    public void agregarCuentaCliente(String documento, String numeroCuenta);

    public List<String> obtenerCuentasCliente(String documento);
}

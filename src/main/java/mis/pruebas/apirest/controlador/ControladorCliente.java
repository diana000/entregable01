package mis.pruebas.apirest.controlador;
import mis.pruebas.apirest.servicio.ServicioCliente;
import mis.pruebas.apirest.modelo.Cliente;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public List<Cliente> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioCliente.obtenerClientes(pagina - 1, cantidad);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarCliente(@RequestBody Cliente cliente)

    {
        this.servicioCliente.insertarClienteNuevo(cliente);

    }

    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento)
    {
        try {
            return this.servicioCliente.obtenerCliente(documento);
        } catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente)
    {
        try{
        cliente.documento = nroDocumento;
        this.servicioCliente.guardarCliente(cliente);
        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @PatchMapping("/{documento}")
    public void emparcharUnCliente(@PathVariable("documento") String nroDocumento, @RequestBody Cliente cliente)
    {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{documento}")
    public void eliminarUnCliente(@PathVariable String documento)
    {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch (Exception x){}
    }


}

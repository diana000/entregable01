package mis.pruebas.apirest.servicio;

import mis.pruebas.apirest.modelo.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();
    public void insertarCuentaNueva(Cuenta cuenta);
    public Cuenta obtenerCuenta(String numero);
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);
    public void borrarCuenta(String numero);



}

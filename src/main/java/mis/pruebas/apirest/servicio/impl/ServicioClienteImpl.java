package mis.pruebas.apirest.servicio.impl;

import mis.pruebas.apirest.modelo.Cliente;
import mis.pruebas.apirest.servicio.ServicioCliente;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioClienteImpl implements ServicioCliente{

    public final Map<String, Cliente> clientes = new ConcurrentHashMap<>();

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad)

    {
        final List<Cliente> clientesContados = List.copyOf(this.clientes.values());
        int indiceIncial = pagina*cantidad;
        int indiceFinal = indiceIncial + cantidad;
        System.out.println((clientesContados.size()));

        if(indiceFinal > clientesContados.size()){
            indiceFinal = clientesContados.size();
        }
        System.out.println("Indice inicial : " + indiceIncial + "Indice final: " + indiceFinal);
        return clientesContados.subList(indiceIncial, indiceFinal);
    }


    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento)
    {
        if(!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        return this.clientes.get(documento);
    }


    @Override
    public void guardarCliente(Cliente cliente) {
        if(!this.clientes.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente: " + cliente.documento);

        final var cuentasCliente = obtenerCuentasCliente(cliente.documento);
        cliente.codigosCuentas = cuentasCliente;
        this.clientes.replace(cliente.documento, cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche)
    {
        final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != existente.edad)
            existente.edad = parche.edad;
        if(parche.nombre != null)
            existente.correo = parche.correo;
        if(parche.direccion != null)
            existente.direccion = parche.direccion;
        if(parche.telefono != null)
            existente.telefono = parche.telefono;
        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientes.replace(existente.documento, existente);
    }


    @Override
    public void borrarCliente(String documento) {
        if(!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        this.clientes.remove(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta)
        {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.codigosCuentas.add(numeroCuenta);
        }

    @Override
    public List<String> obtenerCuentasCliente(String documento)
    {
        final Cliente cliente = this.obtenerCliente(documento);
        return cliente.codigosCuentas;
    }

}
